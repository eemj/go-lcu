package main

import (
	"encoding/json"
	"log"
	"os"
	"os/signal"

	"gitlab.com/eemj/go-lcu"
	"gitlab.com/eemj/go-lcu/proxy"
)

func main() {
	league, err := lcu.NewLeague()

	if err != nil {
		log.Fatal(err)
	}

	defer league.Close()

	s := make(chan os.Signal, 1)
	signal.Notify(s, os.Interrupt)

	if err = league.Proxy.Subscribe("OnJsonApiEvent"); err != nil {
		log.Fatal(err)
	}

	league.Proxy.HandleConnect(func() {
		log.Print("[INFO ] connected")
	})

	league.Proxy.HandleDisconnect(func() {
		log.Print("[INFO ] disconnected")
	})

	league.Proxy.HandleError(func(err error) {
		log.Printf("[ERROR] %v", err)
	})

	league.Proxy.HandleEvent(func(event *proxy.Event) {
		if event.URI == "/lol-matchmaking/v1/ready-check" {
			readyCheck := new(struct {
				PlayerResponse string `json:"playerResponse"`
			})

			if err := json.Unmarshal(event.Data, readyCheck); err != nil {
				return
			}

			switch readyCheck.PlayerResponse {
			case "Accepted", "Declined":
				return
			default:
				res, err := league.Post("/lol-matchmaking/v1/ready-check/accept", nil)

				if err != nil {
					return
				}

				defer res.Body.Close()

				log.Print("[INFO ] accepted")
			}
		}
	})

	<-s
}
