package proxy

const (
	MessageTypeSubscribe   = 5
	MessageTypeUnsubscribe = 6
)

func (p *Proxy) hasSubscription(event string) (exists bool) {
	p.sLock.RLock()
	defer p.sLock.RUnlock()
	_, exists = p.subscriptions[event]
	return
}

func (p *Proxy) Subscribe(event string) (err error) {
	if !p.hasSubscription(event) {
		p.sLock.Lock()
		p.subscriptions[event] = struct{}{}
		p.sLock.Unlock()
	}

	if p.disconnected() {
		return
	}

	return p.conn.WriteJSON(
		[]interface{}{MessageTypeSubscribe, event},
	)
}

func (p *Proxy) SubscribeAll() (err error) {
	if p.disconnected() {
		return
	}

	p.sLock.RLock()
	defer p.sLock.RUnlock()
	for subscription := range p.subscriptions {
		if err = p.Subscribe(subscription); err != nil {
			return
		}
	}

	return
}

func (p *Proxy) Unsubscribe(event string) (err error) {
	if p.hasSubscription(event) {
		p.sLock.Lock()
		delete(p.subscriptions, event)
		p.sLock.Unlock()
	}

	if p.disconnected() {
		return
	}

	return p.conn.WriteJSON([]interface{}{
		MessageTypeUnsubscribe, event,
	})
}

func (p *Proxy) UnsubscribeAll() (err error) {
	if p.disconnected() {
		return
	}

	p.sLock.RLock()
	defer p.sLock.RUnlock()
	for subscription := range p.subscriptions {
		if err = p.Unsubscribe(subscription); err != nil {
			return
		}
	}

	return
}
