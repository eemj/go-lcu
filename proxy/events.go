package proxy

import "encoding/json"

type EventType string

type Event struct {
	Subscription string
	Data         []byte
	EventType    EventType
	URI          string
}

const (
	EventTypeDelete EventType = "Delete"
	EventTypeUpdate EventType = "Update"
	EventTypeCreate EventType = "Create"
)

func BytesToEvent(payload []byte) (ev *Event, err error) {
	data := new([]interface{})

	if err = json.Unmarshal(payload, data); err != nil {
		return
	}

	ev = &Event{
		Subscription: (*data)[1].(string),
	}

	if value, ok := (*data)[2].(map[string]interface{}); ok {
		ev.EventType = EventType(value["eventType"].(string))
		ev.URI = value["uri"].(string)
		data, _ := json.Marshal(value["data"])
		ev.Data = data
	}

	return
}
