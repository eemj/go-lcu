package proxy

import (
	"crypto/tls"
	"net/http"
	"sync"
	"time"

	"gitlab.com/eemj/go-lcu/lockfile"

	"github.com/gorilla/websocket"
)

var (
	upgrader = websocket.Upgrader{
		CheckOrigin:     func(*http.Request) bool { return true },
		ReadBufferSize:  1 << 10,
		WriteBufferSize: 1 << 10,
	}
	dialer = &websocket.Dialer{
		Proxy:            http.ProxyFromEnvironment,
		HandshakeTimeout: 45 * time.Second,
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	}
)

type Proxy struct {
	// the websocket connection which is connected to the league client
	conn  *websocket.Conn
	cLock sync.RWMutex

	// when the websocket connection is established, we'll send out subscribe events
	// the events act like middlewares, when the client recieves or sends something,
	// messages are sent to the connected websocket clients.
	subscriptions map[string]struct{}
	sLock         sync.RWMutex

	// the lockfile connection
	lockfile *lockfile.Lockfile

	// handleConnect triggers when we established a connection
	handleConnect func()

	// handleDisconnect triggers when our connection disconnects
	handleDisconnect func()

	// handleEvent triggers when we received an event
	handleEvent func(*Event)

	// handleError triggers when we received an error
	handleError func(error)
}

func (p *Proxy) HandleConnect(fn func())     { p.handleConnect = fn }
func (p *Proxy) HandleDisconnect(fn func())  { p.handleDisconnect = fn }
func (p *Proxy) HandleEvent(fn func(*Event)) { p.handleEvent = fn }
func (p *Proxy) HandleError(fn func(error))  { p.handleError = fn }

func (p *Proxy) Close() error {
	p.cLock.Lock()
	defer p.cLock.Unlock()
	err := p.conn.Close()
	if err == nil {
		p.handleDisconnect()
	}
	p.conn = nil
	return err
}

func (p *Proxy) disconnected() bool {
	p.cLock.RLock()
	defer p.cLock.RUnlock()
	return p.conn == nil
}

func (p *Proxy) Connect() (err error) {
	ws, _, err := dialer.Dial(
		p.lockfile.WebSocket(), p.lockfile.Headers(),
	)

	if err != nil {
		return
	}

	p.cLock.Lock()
	p.conn = ws
	p.cLock.Unlock()

	// persist subscriptions after connection disconnected
	p.SubscribeAll()

	go func() {
		for {
			if p.disconnected() {
				return
			}

			messageType, msg, err := p.conn.ReadMessage()

			if err != nil {
				p.handleError(err)
				return
			}

			if len(msg) > 0 && messageType == websocket.TextMessage {
				event, err := BytesToEvent(msg)

				if err != nil {
					p.handleError(err)
					continue
				}

				p.handleEvent(event)
			}
		}
	}()

	return
}

func NewProxy(lockfile *lockfile.Lockfile) (proxy *Proxy) {
	proxy = &Proxy{
		lockfile:      lockfile,
		subscriptions: make(map[string]struct{}),
	}

	go func(p *Proxy) {
		for {
			err, ok := <-p.lockfile.Updated()

			if !ok {
				return
			}

			if err != nil && p.conn != nil {
				p.conn.Close()
				(*p).conn = nil
			} else if err == nil && p.lockfile.NotEmpty() {
				if err = p.Connect(); err != nil {
					p.handleError(err)
				} else {
					p.handleConnect()
				}
			}
		}
	}(proxy)

	return
}
