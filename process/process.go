package process

import (
	"errors"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"runtime"
	"strings"
)

var (
	installDirectory *regexp.Regexp = regexp.MustCompile("--install-directory=(.*?) \"?--")

	ErrInstallationDirectoryNotFound error = errors.New("installation directory not found")
	ErrNoInstanceAvailable           error = errors.New("no instance available")
)

func GetInstallationPath() (path string, err error) {
	c := new(exec.Cmd)

	if runtime.GOOS == "windows" {
		c = exec.Command("cmd", "/C", "wmic", "process", "where", "name='LeagueClientUx.exe'", "get", "commandline")
	} else {
		c = exec.Command("sh", "-c", "ps x -o args | grep 'LeagueClientUx'")
	}

	output, err := c.CombinedOutput()

	if err != nil {
		return
	}

	if strings.HasPrefix(strings.ToLower(string(output)), "no instance") {
		err = ErrNoInstanceAvailable
		return
	}

	if !installDirectory.MatchString(string(output)) {
		err = ErrInstallationDirectoryNotFound
		return
	}

	path = strings.TrimSpace(installDirectory.FindStringSubmatch(string(output))[1])

	if strings.HasSuffix(path, "\"") {
		path = path[:len(path)-1]
	}

	if runtime.GOOS == "linux" {
		var homedir string
		homedir, err = os.UserHomeDir()

		if err != nil {
			return
		}

		path = strings.ReplaceAll(path[3:], "\\", "/")
		path = filepath.Join(homedir, ".wine", "drive_c", path) // remove the first 3 characters
	}

	if _, err = os.Stat(path); err != nil && os.IsNotExist(err) {
		err = ErrInstallationDirectoryNotFound
	}

	return
}
