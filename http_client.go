package lcu

import (
	"crypto/tls"
	"io"
	"net/http"

	"gitlab.com/eemj/go-lcu/lockfile"
)

type headerTransport struct {
	tripper  http.RoundTripper
	lockfile *lockfile.Lockfile
	username string
}

func newHeaderTransport(
	lockfile *lockfile.Lockfile,
	username string,
) *headerTransport {
	defaultTransport := http.DefaultTransport.(*http.Transport)
	defaultTransport.TLSClientConfig = &tls.Config{InsecureSkipVerify: true}

	return &headerTransport{
		tripper:  defaultTransport,
		lockfile: lockfile,
		username: "riot",
	}
}

func (h *headerTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	req.SetBasicAuth(h.username, h.lockfile.Passphrase)
	return h.tripper.RoundTrip(req)
}

func (l *League) Get(path string) (res *http.Response, err error) {
	req, err := http.NewRequest(http.MethodGet, (l.Lockfile.URI() + path), nil)

	if err != nil {
		return nil, err
	}

	return l.HTTPClient.Do(req)
}

func (l *League) Post(path string, body io.Reader) (res *http.Response, err error) {
	req, err := http.NewRequest(http.MethodPost, (l.Lockfile.URI() + path), body)

	if err != nil {
		return nil, err
	}

	return l.HTTPClient.Do(req)
}
