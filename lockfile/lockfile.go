package lockfile

import (
	"encoding/base64"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

const WatchInterval time.Duration = 2 * time.Second

var ErrInvalidLockfilePathSpecified error = errors.New("Invalid lockfile path specified.")

type Lockfile struct {
	Path       string
	Passphrase string
	Protocol   string
	PID        int
	Port       int
	User       string

	updatec chan error
	size    int64
	modTime time.Time
}

func (l *Lockfile) URI() string { return fmt.Sprintf("%s://127.0.0.1:%d", l.Protocol, l.Port) }

func (l *Lockfile) WebSocket() string { return fmt.Sprintf("wss://127.0.0.1:%d/", l.Port) }

func (l *Lockfile) NotEmpty() bool { return len(l.Protocol) > 0 && len(l.Passphrase) > 0 }

func (l *Lockfile) Headers() http.Header {
	return http.Header{
		"Authorization": []string{
			"Basic " + l.Encoded(),
		},
	}
}

func (l *Lockfile) Encoded() string {
	return base64.StdEncoding.EncodeToString([]byte(
		l.User + ":" + l.Passphrase,
	))
}

func NewLockfile(path string) (lockfile *Lockfile, err error) {
	if len(strings.TrimSpace(path)) == 0 {
		return nil, ErrInvalidLockfilePathSpecified
	}

	lockfile = &Lockfile{
		updatec: make(chan error),
		User:    "riot",
		Path:    path,
	}

	lockfile.Watch()

	return
}

func (l *Lockfile) Updated() <-chan error { return l.updatec }

func (l *Lockfile) Update() (err error) {
	stat, err := os.Stat(l.Path)

	if err != nil && os.IsNotExist(err) {
		return
	}

	l.modTime = stat.ModTime()
	l.size = stat.Size()

	contents, err := ioutil.ReadFile(l.Path)

	if err != nil {
		return
	}

	splitContents := strings.Split(string(contents), ":")

	l.Passphrase = splitContents[3]
	l.Protocol = splitContents[4]

	port, err := strconv.Atoi(splitContents[2])

	if err != nil {
		return
	}

	l.Port = port

	pid, err := strconv.Atoi(splitContents[1])

	if err != nil {
		return
	}

	l.PID = pid

	return
}

func (l *Lockfile) Watch() {
	go func() {
		initial := &Lockfile{}
		ticker := time.NewTicker(WatchInterval)

		defer ticker.Stop()

		for ; true; <-ticker.C {
			stat, err := os.Stat(l.Path)

			// the lockfile got removed by the client, meaning the client closed.
			if err != nil && os.IsNotExist(err) && l.NotEmpty() {
				l.size = 0
				l.modTime = time.Time{}
				l.Protocol = ""
				l.Passphrase = ""
				l.Port = 0
				l.PID = 0

				l.updatec <- err
			} else if err != nil && !os.IsNotExist(err) {
				return
			} else if initial == nil ||
				(initial != nil &&
					stat != nil &&
					stat.Size() != initial.size ||
					stat.ModTime() != initial.modTime) {
				l.Update()
				(*initial) = (*l)
				l.updatec <- nil
			}
		}
	}()
}
