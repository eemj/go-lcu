package lcu

import (
	"net/http"
	"path/filepath"

	"gitlab.com/eemj/go-lcu/lockfile"
	"gitlab.com/eemj/go-lcu/process"
	"gitlab.com/eemj/go-lcu/proxy"
)

type League struct {
	InstallDir string
	Lockfile   *lockfile.Lockfile
	Proxy      *proxy.Proxy
	HTTPClient *http.Client
}

func (l *League) Close() (err error) {
	// not sure if this is really needed, but better safe than sorry
	if err = l.Proxy.UnsubscribeAll(); err != nil {
		return
	}

	return l.Proxy.Close()
}

func NewLeague() (league *League, err error) {
	installDir, err := process.GetInstallationPath()

	if err != nil {
		return
	}

	lockfile, err := lockfile.NewLockfile(
		filepath.Join(installDir, "lockfile"),
	)

	if err != nil {
		return
	}

	league = &League{
		InstallDir: installDir,
		Lockfile:   lockfile,
		Proxy:      proxy.NewProxy(lockfile),
		HTTPClient: &http.Client{
			Transport: newHeaderTransport(lockfile, "riot"),
		},
	}

	return
}
